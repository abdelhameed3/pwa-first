import { Component } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { MessagingService } from "./service/messaging.service";
import { ToastrService } from 'ngx-toastr';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "push-notification";
  message;
  status = "Online";
  isConnected = true;
  constructor(public messagingService: MessagingService,
    private swUpdates: SwUpdate) {


     }
ngOnInit() {
  this.messagingService.requestPermission()
  this.messagingService.receiveMessage()
  this.message = this.messagingService.currentMessage;
  this.reloadCache();
 }

 reloadCache(){
  if(this.swUpdates.isEnabled){
    this.swUpdates.available.subscribe( ()=>{
      if(confirm('New Version available! Would you like to update?')){
        window.location.reload();
      }
    })
  }
 }




 async send(){
   try{
     var req = this.messagingService;
      setTimeout( function(){const res =  req.sendMessage();},5000)
   } catch(err){
    console.log('err',err);
   }
 }
}
