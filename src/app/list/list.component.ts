import { ApiService } from './../service/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  comments:any =[];
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.getComment();
  }

  async getComment(){
    try{
      const res:any =  await this.apiService.get();

      this.comments = res;
      console.log('comments',this.comments);
    } catch(err){
      console.log('err',err);
    }
  }

}
