import { ToastrService } from 'ngx-toastr';
import { MessagingService } from './../service/messaging.service';
import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-new-visit',
  templateUrl: './new-visit.component.html',
  styleUrls: ['./new-visit.component.scss'],
})
export class NewVisitComponent implements OnInit {
  farmObj: any = {};
  currentRate = 8;
  form: FormGroup;
  formIndex;

  uploadedFile2: any = [];
  public fileData2: any = null;
  fileList: File[] = [];
  filesObject: any = [];
  fileChanged2 = false;
  urls = [];
  get rate() {
    return this.form.get('rate') as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    public messagingService: MessagingService,
    private sanitizer: DomSanitizer,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }
  buildForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      date: [null],
      rate: this.fb.array([
        this.fb.group({
          question: [
            ' Authorized cleaning/disinfectant products and equipment are used',
          ],
          rateValue: [],
          comment: [],
          photo: this.fb.array([]),
        }),
        this.fb.group({
          question: [
            ' Cleaning water controlled (refer to water quality guideline) and in sufficient quantity - Tanks & Parlor',
          ],
          rateValue: [],
          comment: [],
          photo: this.fb.array([]),
        }),
        this.fb.group({
          question: [
            '  Satisfactory cleaning of the equipment and general cleanliness of the milking parlour/waiting area',
          ],
          rateValue: [],
          comment: [],
          photo: this.fb.array([]),
        }),
        this.fb.group({
          question: [
            ' Regular PM (Annual, Monthly & Weekly) of the milking machine in place.',
          ],
          rateValue: [],
          comment: [],
          photo: this.fb.array([]),
        }),
      ]),
    });
  }
  formInitData() {
    return {
      name: '',
      date: null,
      rate: {
        question: '',
        rateValue: '',
        comment: '',
        photo: '',
      },
    };
  }

  resetData() {
    this.form.reset(this.formInitData());
    this.form.updateValueAndValidity();
  }
  open(content, index) {
    let photo = this.rate.controls[index].get('photo') as FormArray;
    this.fileData2 = [];
    this.fileChanged2 = false;
    this.fileList= [];
    this.urls = [];
    for(let i = 0;  i < photo.length ; i++){
      this.urls.push('data:image/png;base64,' +photo.controls[i].value.document);
    }
    this.filesObject= [];

    this.modalService.open(content, { size: 'lg' });
    this.formIndex = index;
  }

  saveForm() {
    if (this.messagingService.isConnected) {
      this.toastr.success('Sended To  API', 'Success');
    } else {
      this.toastr.warning('Saved  in Cache', 'Offline Mode');
      localStorage.setItem('FormCache', JSON.stringify(this.form.value));
    }
  }

  uploadFile2($event) {
    // if ($event.target.files.length > 20 || this.urls.length > 20) {
    //     this.helper.errorAlert('You can only upload a maximum of 20 Images');
    // } else {
    this.fileData2 = $event.target.files;
    this.fileChanged2 = true;
    for (let i = 0; i < $event.target.files.length; i++) {
      let reader = new FileReader();
      this.fileList.push(this.fileData2[i]);
      reader.readAsDataURL($event.target.files[i]);
      let photo = this.rate.controls[this.formIndex].get('photo') as FormArray;

      reader.onload = (event: any) => {
        this.urls.push(event.target.result);


        photo.push(
          this.fb.group({
            name: [this.fileData2[i].name],
            document: [ event.target.result.split("base64,")[1]],
            documentSize:[this.fileData2[i].size],
            documentType:[this.fileData2[i].type],
          })
        )
      };
    }




  }

  removeImage(index) {
    this.fileList.splice(index, 1);
    this.urls.splice(index, 1);
    let photo = this.rate.controls[this.formIndex].get('photo') as FormArray;
    photo.removeAt(index)
  }

  clearFile2() {
    this.fileData2 = [];
    this.uploadedFile2 = [];
  }

}
