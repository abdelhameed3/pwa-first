import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{

  constructor( private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
          setHeaders: {
            Authorization: `key=AAAAOw7TPQs:APA91bEWyFJv6T99jeGjDepyM1nb8sfCuJJrIHm8XuPD1EJ4r6Gv4hDSqs0b7jtxnE2tVb-AYQwGwLwIDIg2GRsn_5D_RcxjvXR0z1LDqKbOl9LzSKC1Ny4gUqAmyxegtWEfJUxzpn0F`
          }
        });
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
         // console.log('event--->>>', event.body);
          if (event) {
            if (event.body.message && event.body.message.indexOf('Token') > -1 && event.body.isPassed == false)

              this.router.navigate(['/login']);
          }
        }
        return event;
      }));
    }
}
