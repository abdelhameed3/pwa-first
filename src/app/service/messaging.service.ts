import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { ConnectionService } from 'ng-connection-service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  token;
  status = "Online";
  isConnected ;
  constructor(private angularFireMessaging: AngularFireMessaging,
    private connectionService: ConnectionService,
    private toastr: ToastrService,
    private http: HttpClient) {
    // this.angularFireMessaging.messaging.subscribe((_messaging) => {
    //   _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    //   _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    // });
    let status = navigator.onLine;
   this.isConnected = status;
   if (this.isConnected) {
    this.status = "Online";
    this.toastr.success('You Are Online Now!', 'Online Mode');
    let cache = localStorage.getItem('FormCache');
    if(cache){
       this.toastr.success('Sended To  API','Success');
       localStorage.removeItem('FormCache');
    }

  }
  else {
    this.toastr.error('You Are Offline Now!', 'Offline Mode');
    this.status = "Offline";
  }
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.status = "Online";
        this.toastr.success('You Are Online Now!', 'Online Mode');
        let cache = localStorage.getItem('FormCache');
        if(cache){
           this.toastr.success('Sended To  API','Success');
           localStorage.removeItem('FormCache');
        }

      }
      else {
        this.toastr.error('You Are Offline Now!', 'Offline Mode');
        this.status = "Offline";
      }
    })
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
          this.token = token;
          console.log(token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe((payload) => {
      console.log('new message received. ', payload);
      this.currentMessage.next(payload);
    });
  }


  sendMessage() {
    return this.http.post(`https://fcm.googleapis.com/fcm/send`, {
      notification: {
        title: "Hey Ya man",
      body: "Welcome From My first Notification"
      },
      to : this.token
     }).toPromise();

  }
}
