// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAn5mi3Pr00fhmJC6IMbM54PzdY5EMDFYU",
    authDomain: "notification-9bb69.firebaseapp.com",
    projectId: "notification-9bb69",
    storageBucket: "notification-9bb69.appspot.com",
    messagingSenderId: "253651795211",
    appId: "1:253651795211:web:48694a37d876a91430f60e",
    measurementId: "G-DMLXLD22MY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
